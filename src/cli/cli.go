package cli

import (
	"errors"
	"flag"
	"fmt"
	"gitlab.com/NateSchreiner/scli/src/scli"
	"regexp"
)

// scli set getzs Pwd.4getzs

var master *string

func init() {
	master = flag.String("m", "", "Provide a master key to decrypt file")
}

func Run(args []string) int {
	app, err := NewApp(args)
	if err != nil {
		fmt.Println(err)
		return 1
	}
	return app.Run()
}

func NewApp(args []string) (*scli.AppCommand, error) {
	command, err := parseCommand(args)
	if err != nil {
		return &scli.AppCommand{}, err
	}

	key, err := parseKey(args)
	if err != nil {
		return &scli.AppCommand{}, err
	}

	secret, err := parseSecret(args)

	master, err := parseMasterPass(args)
	if err != nil {
		return &scli.AppCommand{}, err
	}

	app := &scli.AppCommand{
		Name:   command,
		Key:    key,
		Secret: secret,
		Master: master,
	}

	return app, nil
}

func parseCommand(args []string) (string, error) {
	for _, arg := range args {
		if arg == "get" || arg == "set" {
			return arg, nil
		}
	}

	return "", errors.New("no command specified")
}

func parseKey(args []string) (string, error) {
	if len(args) < 2 {
		return "", errors.New("no key specified")
	}

	if checkArgForFlag(args[1]) {
		return "", errors.New("second argument seems to be a flag key expected")
	}

	return args[1], nil
}

func parseSecret(args []string) (string, error) {
	if args[0] == "get" {
		return "", nil
	}

	if len(args) < 3 {
		return "", errors.New("not enough arguments supplied to set command")
	}

	if checkArgForFlag(args[2]) {
		return "", errors.New("second argument seems to be a flag key expected")
	}

	return args[2], nil
}

func parseMasterPass(args []string) (string, error) {
	for index, arg := range args {
		if checkArgForFlag(arg) {
			if len(args) > index+1 {
				return args[index+1], nil
			}

			return "", errors.New("master password not specified")
		}
	}

	return "", nil
}

func checkArgForFlag(arg string) bool {
	match, err := regexp.Match("^-", []byte(arg))
	if err != nil {
		panic(err)
	}

	return match
}
