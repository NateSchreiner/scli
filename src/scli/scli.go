package scli

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"strings"
	"time"
)

const timeoutFile = "timeout_timestamp"
const secretsFile = "secrets"
const masterFile = "master"
const directory = "/.scli/"
const testString = "masterpasswordteststring"

/*
	scli -sm password4scli -l /Users/nateschreiner/.ssh/
	^ Would set the master password in file created at provided location
*/

// AppCommand :
// Set <key> <password> <MasterKey>?
// Get <key> <MasterKey>?
// Set -m <key>
// Set -s <Scheme> <key> <password> <MasterKey>?
type AppCommand struct {
	Name   string
	Key    string
	Secret string
	Master string
}

type FileCreationError struct {
	Filename string
}

func (s *FileCreationError) Error() string {
	return fmt.Sprintf("Failed to create %s", s.Filename)
}

type DirCreationError struct {
	DirName string
}

func (s *DirCreationError) Error() string {
	return fmt.Sprintf("Failed to create %s", s.DirName)
}

type SecretsFileFailure struct{}

func (s *SecretsFileFailure) Error() string {
	return fmt.Sprint("Failed to create or read secrets file")
}

func (ap *AppCommand) Run() int {
	home, err := os.UserHomeDir()
	if err != nil {
		panic("can't find home directory")
	}

	err = checkAndSetupFiles(home)
	if err != nil {
		switch err.(type) {
		case *DirCreationError:
			fmt.Println(err.Error())
			return 1
		case *SecretsFileFailure:
			fmt.Println(err.Error())
			return 1
		}
	}

	timeout, err := readTimeoutFile(home)
	if err != nil {
		deleteMasterFile(home)
	}

	timestamp, err := time.Parse(time.RFC822, timeout)
	if err != nil {
		deleteMasterFile(home)
		os.Remove(home + directory + timeoutFile)
	}

	if !checkTimeoutTimestamp(timestamp) {
		deleteMasterFile(home)
	}

	master, fromUser := getMasterPassword(home, ap.Master)

	result := checkMasterPassword(master, home)

	if !result {
		fmt.Println("User supplied master pass is wrong")
		return 1
	}

	if fromUser {
		err := updateMasterFile(master, home)
		if err != nil {
			fmt.Println(err.Error())
			return 1
		}

		result := updateTimeoutFile(home)
		if result != 0 {
			fmt.Println("Issue with timeout file")
			return 1
		}
	}

	return ap.runCommand(home, master)
}

func checkTimeoutTimestamp(value time.Time) bool {
	now := time.Now().Local()
	return now.Before(value)
}

func readTimeoutFile(home string) (string, error) {

	data, err := os.ReadFile(home + directory + timeoutFile)
	if err != nil {
		return "", err
	}

	parts := strings.Split(string(data), "\n")
	if len(parts) < 1 {
		return "", nil
	}

	return parts[0], nil
}

func updateTimeoutFile(home string) int {
	_, err := os.Stat(home + directory + timeoutFile)
	if err == nil {
		err = os.Remove(home + directory + timeoutFile)
		if err != nil {
			panic("Can not delete timeout file")
		}
	}

	file, err := os.OpenFile(home+directory+timeoutFile, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		fmt.Println("Can't create timeout file")
		return 1
	}

	defer file.Close()

	timein := time.Now().Local().Add(time.Duration(15) * time.Minute)
	_, err = file.WriteString(timein.Format(time.RFC822))
	if err != nil {
		fmt.Println("Could not write timestamp to file")
		return 1
	}

	return 0
}

func updateMasterFile(master string, home string) error {
	_, err := os.Stat(home + directory + masterFile)
	if err != nil {
		// NO master file
		return createMasterFile(master, home)
	}

	// master file
	os.Remove(home + directory + masterFile)

	return createMasterFile(master, home)
}

func createMasterFile(master string, home string) error {
	file, err := os.OpenFile(home+directory+masterFile, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		return err
	}

	defer file.Close()

	_, err = file.WriteString(master)
	if err != nil {
		return err
	}

	return nil
}

func checkMasterPassword(master string, home string) bool {
	data, err := os.ReadFile(home + directory + secretsFile)
	if err != nil {
		return false
	}

	if len(data) == 0 {
		// First time here? Add the encrypted test string
		file, err := os.OpenFile(home+directory+secretsFile, os.O_CREATE|os.O_RDWR, 0666)
		if err != nil {
			return false
		}

		defer file.Close()

		encrypted, err := Encrypt(master, testString)
		if err != nil {
			os.Remove(home + directory + secretsFile)
			return false
		}

		_, err = file.WriteString(encrypted)
		if err != nil {
			os.Remove(home + directory + secretsFile)
			return false
		} else {
			return true
		}
	}

	parts := strings.Split(string(data), "\n")

	unencrypted, err := Decrypt(master, parts[0])
	if err != nil {
		return false
	}

	return unencrypted == testString
}

func getMasterPassword(home string, supplied string) (string, bool) {
	var master string
	master, err := readMasterPassword(home)
	if err != nil {
		if supplied == "" {
			master = getMasterPasswordFromUser()
			return master, true
		} else {
			return supplied, true
		}
	}

	if master == "" {
		master = getMasterPasswordFromUser()
		return master, true
	}

	return master, false
}

func getMasterPasswordFromUser() string {
	fmt.Println("Enter your master password: ")

	var master string
	fmt.Scanln(&master)

	return master
}

func deleteMasterFile(home string) {
	err := os.Remove(home + directory + masterFile)
	if err != nil {
		// check if file even
		_, err = os.Stat(home + directory + masterFile)
		if err == nil {
			os.Remove(home + directory + masterFile)
		}
	}
}

func readMasterPassword(home string) (string, error) {
	data, err := os.ReadFile(home + directory + masterFile)
	if err != nil {
		return "", errors.New("master password file not found")
	}

	parts := strings.Split(string(data), "\n")
	if len(parts) < 1 {
		return "", errors.New("no password found")
	}

	return parts[0], nil
}

func checkAndSetupFiles(home string) error {

	// Check for the presence of a master password file
	_, err := os.Stat(home + directory)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			dirResult := createMainDir(home)
			if dirResult != 0 {
				return &DirCreationError{DirName: home + directory}
			}
		} else {
			return err
		}
	}

	var masterResult int
	var timeoutResult int
	var secretsResult int

	_, err = os.Stat(home + directory + masterFile)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			masterResult = createEmptyMasterFile(home)
		}
	}

	_, err = os.Stat(home + directory + timeoutFile)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			timeoutResult = createTimeoutFile(home, "")
		}
	}

	_, err = os.Stat(home + directory + secretsFile)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			secretsResult = createSecretsFile(home)
		}
	}

	if secretsResult != 0 {
		return &SecretsFileFailure{}
	}

	if timeoutResult != 0 {
		return &FileCreationError{home + directory + timeoutFile}
	}
	if masterResult != 0 {
		return &FileCreationError{home + directory + masterFile}
	}

	return nil
}

func createSecretsFile(home string) int {
	file, err := os.OpenFile(home+directory+secretsFile, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		return 1
	}

	file.Close()

	return 0
}

func createMainDir(home string) int {
	err := os.Mkdir(home+directory, 0700)
	if err != nil {
		return 1
	}

	return 0
}

func createTimeoutFile(home string, time string) int {
	file, err := os.OpenFile(home+directory+timeoutFile, os.O_CREATE, 0666)
	if err != nil {
		return 1
	}
	defer file.Close()

	file.WriteString(time)

	return 0
}

func createEmptyMasterFile(home string) int {
	file, err := os.OpenFile(home+directory+masterFile, os.O_CREATE, 0666)
	if err != nil {
		return 1
	}

	file.Close()

	return 0
}

func (ap *AppCommand) runCommand(home string, master string) int {
	if ap.Name == "set" {
		err := set(ap.Key, ap.Secret, master, home)
		if err != nil {
			return 1
		}
		fmt.Println("Successfully set key")
		return 0
	} else {
		secret, err := get(ap.Key, master, home)
		if err != nil {
			return 1
		}

		fmt.Println(secret)
		return 0
	}
}

func set(key string, value string, master string, home string) error {
	file, err := os.OpenFile(home+directory+secretsFile,
		os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		return errors.New("error opening secrets file")
	}

	defer file.Close()
	var line strings.Builder
	line.WriteString(fmt.Sprintf("%s ", key))

	encrypted, err := Encrypt(master, value)
	if err != nil {
		return errors.New("error encrypting secret")
	}
	line.WriteString(encrypted)

	_, err = file.WriteString("\n")
	if err != nil {
		return errors.New("error writing newline to secrets file")
	}
	_, err = file.WriteString(line.String())
	if err != nil {
		return errors.New("error writing line to secrets file")
	}

	return nil
}

func get(key string, master string, home string) (string, error) {
	var foundKey = false
	data, err := os.ReadFile(home + directory + secretsFile)
	if err != nil {
		return "", err
	}

	parts := strings.Split(string(data), "\n")
	for _, line := range parts {
		keyValue := strings.Split(line, " ")
		if keyValue[0] == key {
			foundKey = true
			unencrypted, err := Decrypt(master, keyValue[1])
			if err != nil {
				return "", err
			}

			return unencrypted, nil
		}
	}

	if !foundKey {
		return "", errors.New("key not found in file")
	}

	return "", err
}

// Encrypt will take in a key and plaintext and return a hex representation
// of the encrypted value.
// This code is based on the standard library examples at:
//   - https://golang.org/pkg/crypto/cipher/#NewCFBEncrypter
func Encrypt(key, plaintext string) (string, error) {
	block, err := newCipherBlock(key)
	if err != nil {
		return "", err
	}

	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], []byte(plaintext))

	return fmt.Sprintf("%x", ciphertext), nil
}

// Decrypt will take in a key and a cipherHex (hex representation of
// the ciphertext) and decrypt it.
// This code is based on the standard library examples at:
//   - https://golang.org/pkg/crypto/cipher/#NewCFBDecrypter
func Decrypt(key, cipherHex string) (string, error) {
	block, err := newCipherBlock(key)
	if err != nil {
		return "", err
	}

	ciphertext, err := hex.DecodeString(cipherHex)
	if err != nil {
		return "", err
	}

	if len(ciphertext) < aes.BlockSize {
		return "", errors.New("encrypt: cipher too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(ciphertext, ciphertext)
	return string(ciphertext), nil
}

func newCipherBlock(key string) (cipher.Block, error) {
	hasher := md5.New()
	fmt.Fprint(hasher, key)
	cipherKey := hasher.Sum(nil)
	return aes.NewCipher(cipherKey)
}
