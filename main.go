package main

import (
	"gitlab.com/NateSchreiner/scli/src/cli"
	"os"
)

func main() {

	// cli saves sudo password by writing to a file with a timeout timestamp at which point
	// the shell will "forget" the sudo password by `deleting tmp file`?
	//
	// We would want another command line function triggered by the os to delete this file at given timestamps
	os.Exit(cli.Run(os.Args[1:]))
}
